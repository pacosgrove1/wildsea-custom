import { WILDSEA } from '../../systems/wildsea/system/config.js'

const WILDSEACUSTOM = {
  edges: ['lorem', 'ipsum', 'dolor', 'sit'],
  skills: ['lorem', 'ipsum', 'dolor', 'sit'],
  languages: ['lorem', 'ipsum', 'dolor', 'sit'],
}

Hooks.once('init', () => {
  WILDSEA.edges = WILDSEACUSTOM.edges
  WILDSEA.skills = WILDSEACUSTOM.skills
  WILDSEA.languages = WILDSEACUSTOM.languages
})
